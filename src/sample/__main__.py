import sys

from .simple import add_one

def main(arg):
    '''Entry point for the application script'''
    print(add_one(int(arg)))

if __name__ == '__main__':
    main(sys.argv[1])
