*** Settings ***
Library    OperatingSystem

*** Test Cases ***
Sample should start from command line
    Given we want to increment number 2
    When sample is invoked
    Then output should be 3

*** Keywords ***
We want to increment number ${arg}
    Set test variable    ${ARGUMENT}    ${arg}

Sample is invoked
    ${output}=    Run    python -m sample ${ARGUMENT}
    Set test variable    ${RESULT OUTPUT}    ${output}

Output should be ${expected}
    Should be equal as numbers    ${RESULT OUTPUT}    ${expected}
